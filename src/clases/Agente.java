/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author miguel
 */
public class Agente extends Persona {
    
    
    private short numeroPlaca;

    public Agente(short numeroPlaca, long identificacion, String nombre, String apellido) throws Exception  {
        super(identificacion, nombre, apellido);
        if(numeroPlaca > 999){
            throw new Exception("numero de placa no puede tener mas de 3 digitos");
        }
        if (nombre == null || "".equals(nombre.trim())) {
            throw new Exception("nombre  no puede ser un dato nulo");
        }
          if (apellido == null || "".equals(apellido.trim())) {
            throw new Exception("apellido  no puede ser un dato nulo");
        }
        this.numeroPlaca = numeroPlaca;
    }

    public short getNumeroPlaca() {
        return numeroPlaca;
    }

   
    
    
 

   
    
    
    
}
