/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author miguel
 */
public class MotivoMulta {
    
    private short codigo;
    private String descripcion;
    private int valor;

    public MotivoMulta(short codigo, String descripcion, int valor) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.valor = valor;
    }

    public short getCodigo() {
        return codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public int getValor() {
        return valor;
    }

    public void setCodigo(short codigo) throws Exception {
        String codigo1 = Integer.toString(codigo);        
        if (codigo1 == null ) {
            throw new Exception("codigo no puede ser nulo");
        }}

    public void setDescripcion(String descripcion) throws Exception{
        if(descripcion == null || "".equals(descripcion.trim())) {
            throw new Exception("descripcion no puede ser nulo");
        }
        this.descripcion = descripcion;
    }

    public void setValor(int valor) throws Exception {
        String valor1 = Integer.toString(valor);        
        if (valor1 == null ) {
            throw new Exception("valor  no puede ser nulo");
        }
       this.valor = valor; 
    }
        
    }
    
    
    
    
    
    

