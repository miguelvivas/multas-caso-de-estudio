/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;



/**
 *
 * @author miguel
 */
public class Vehiculo {
    
    private String placa;
    private short modelo;
    private String color;
    private String marca;
    
    public Vehiculo(String placa,short modelo,String color,String marca)throws Exception{
        if(placa.length() > 6){
            throw new Exception("error numero de placa ");
        }  
        
        if (color == null || "".equals(color.trim())) {
            throw new Exception("color  no puede ser un dato nulo");
        }
        if (marca == null || "".equals(color.trim())) {
            throw new Exception("marca no puede ser un dato nulo");
            
            
        }
        if (placa == null || "".equals(placa.trim())) {
            throw new Exception("placa no puede ser nulo");
        }
        
    
       
        
    this.placa = placa;
    this.modelo = modelo;
    this.color = color;
    this.marca = marca;
    }

    public String getPlaca() {
        return placa;
    }

    public short getModelo() {
        return modelo;
    }

    public String getColor() {
        return color;
    }

    public String getMarca() {
        return marca;
    
    }

    public void setPlaca(String placa) throws Exception {
        if (placa == null || "".equals(placa.trim())) {
            throw new Exception("placa no puede ser nulo");
        }
        this.placa = placa; 
    }
    
    
    
       
    }
    
    
    


    
    
    
    

