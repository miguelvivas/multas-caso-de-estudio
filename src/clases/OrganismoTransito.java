/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.util.LinkedList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author miguel
 */
public class OrganismoTransito {

    private LinkedList<Agente> agentes;
    private LinkedList<Licencia> licencias;
    private LinkedList<MotivoMulta> motivomulta;
    private LinkedList<Multa> multa;
    private LinkedList<Vehiculo> vehiculos;

    public OrganismoTransito() {
        this.agentes = new LinkedList();
        this.licencias = new LinkedList();
        this.motivomulta = new LinkedList();
        this.multa = new LinkedList();
        this.vehiculos = new LinkedList();
    }

    public LinkedList<Multa> getMulta() {
        return multa;
    }

    public LinkedList<MotivoMulta> getMotivoMulta() {
        return motivomulta;
    }

    public void agregarMulta(Multa multa) {
        this.multa.add(multa);
    }

    /**
     * **************************************Licencia*****************
     */
    public void agregarLicencia(Licencia identificacion) throws Exception {
        for (Licencia licencia : this.licencias) {
            if (identificacion.getPersona().getIdentificacion() == identificacion.getPersona().getIdentificacion()) {
                throw new Exception("Ya existe la licencia ");
            }
        }
        this.licencias.add(identificacion);
    }

    public Licencia buscarLicencia(long identificacion) throws Exception {
        for (Licencia licencia : this.licencias) {
            if (identificacion == licencia.getPersona().getIdentificacion()) {
                return licencia;
            }
        }
        return null;
    }

    public List<Licencia> buscarLicencias(long identificacion) throws Exception {
        LinkedList<Licencia> encontradas = new LinkedList<>();
        for (Licencia licencia : this.licencias) {
            if (licencia.getPersona().getIdentificacion() == identificacion) {
                encontradas.add(licencia);
            }
        }
        return encontradas;
    }

    /**
     * **********************************Agente***********************************
     */
    public void agregarAgente(Agente agent) throws Exception {
        for (Agente agente : this.agentes) {
            if (agent.getNumeroPlaca() == agente.getNumeroPlaca()) {
                throw new Exception("Ya existe agente con placa " + agente.getNumeroPlaca() + agente.getNombre());
            }
        }
        this.agentes.add(agent);
    }

    public Agente mostrarAgente(short numeroPlaca) {
        for (Agente agente : this.agentes) {
            if (numeroPlaca == agente.getNumeroPlaca()) {
                return agente;
            }
        }
        JOptionPane.showMessageDialog(null, "No existe agente con esta placa");
        return null;
    }

    /**
     * *********************************************Multa***************
     */
    public Multa mostrarMulta(long identificacion) throws Exception {
        for (Multa multa : this.multa) {
            if (identificacion == multa.getPersona().getIdentificacion()) {
                return multa;
            }
        }
        throw new Exception("no existe multa con esa identificacion ");
        // JOptionPane.showMessageDialog(null, "No existe multa");
        //return null;
    }

    /**
     * ******************************Vehiculo******************************
     */
    public void agregarVehiculo(Vehiculo vehi) throws Exception {
        for (Vehiculo vehiculo : this.vehiculos) {
            if (vehi.getPlaca().equals(vehiculo.getPlaca())) {
                throw new Exception("Ya existe vehiculo con placa " + vehi.getPlaca());
            }
        }
        this.vehiculos.add(vehi);
    }

    public Vehiculo buscarVehiculo(String placa) {
        for (Vehiculo vehiculo : this.vehiculos) {
            if (placa.equals(vehiculo.getPlaca())) {
                return vehiculo;
            }
        }
        JOptionPane.showMessageDialog(null, "No existe vehiculo con esta placa");
        return null;
    }

    /**
     * **************************************MotivoMulta*************
     */
    public MotivoMulta devolverMotivoMulta(short codigo) {
        for (MotivoMulta motivoMulta : this.motivomulta) {
            if (codigo == motivoMulta.getCodigo()) {
                return motivoMulta;
            }
        }
        JOptionPane.showMessageDialog(null, "No existe motivo multa");
        return null;
    }

    public void agregarMotivoMulta(MotivoMulta motivoMulta) {
        this.motivomulta.add(motivoMulta);
    }

}
