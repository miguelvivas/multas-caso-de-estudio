/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author miguel
 */
public class Persona {

    private long identificacion;
    private String nombre;
    private String apellido;

    public Persona(long identificacion, String nombre, String apellido) throws Exception{
        if(identificacion < 1000000 || identificacion > 13000000000L){
            throw new Exception("error (numero de identificacion)");
        }      
        this.identificacion = identificacion;
        this.nombre = nombre;
        this.apellido = apellido;
    }

    public long getIdentificacion() throws Exception   {
        
        return identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setNombre(String nombre) throws Exception {
        if (nombre == null || "".equals(nombre.trim())) {
            throw new Exception("el nombre no puede ser nulo");
        }
        this.nombre = nombre;
    }

    public void setApellido(String apellido) throws Exception {
        if (apellido == null || "".equals(apellido.trim())) {
            throw new Exception("el apellido no puede ser nulo");
        }
        this.apellido = apellido;
    }
    
    
    
    

}
