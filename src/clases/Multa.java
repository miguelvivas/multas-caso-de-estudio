/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.util.LinkedList;
import java.util.Date;

/**
 *
 * @author miguel
 */
public class Multa {
    
    
    private int valor;
    private LinkedList<MotivoMulta>motivosmulta;
    private Agente agente;
    private Persona persona;
    private Vehiculo vehiculo;
    private Date fecha;

    public Multa(int valor,  Agente agente, Persona persona, Vehiculo vehiculo,Date fecha) throws Exception{
        if(valor == 0){
            throw new Exception("El valor de la multa no puede ser igual a cero ");
        }      
        this.valor = valor;
        this.motivosmulta = new LinkedList();
        this.agente = agente;
        this.persona = persona;
        this.vehiculo = vehiculo;
        this.fecha = fecha;
    }

   

    public int getValor() {
        return valor;
    }

    public LinkedList<MotivoMulta> getMotivosMulta() {
        return motivosmulta;
    }

    public Agente getAgente() {
        return agente;
    }

    public Persona getPersona() {
        return persona;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

  
    public Date getFecha(){
    return fecha;}
    
    
    
    
    
    
    
    
}
