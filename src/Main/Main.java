/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import clases.Agente;
import clases.Categoria;
import clases.Licencia;
import clases.MotivoMulta;
import clases.Multa;
import clases.OrganismoTransito;
import clases.Persona;
import clases.Vehiculo;
import java.text.SimpleDateFormat;
import java.util.Date;
import ui.Principal;

/**
 *
 *
 *
 *
 * @author miguel
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {

        OrganismoTransito orgt = new OrganismoTransito();

        Date fecha = new Date();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

        fecha = sdf.parse("2016/01/01");

        Agente agente1 = new Agente((short) 123, 1113681204, "andres", "ramirez");
        Agente agente2 = new Agente((short) 145, 16235892, "carlos", "romero");

        Persona persona1 = new Persona(1113682504, "miguel", "vivas");

        Vehiculo vehiculo1 = new Vehiculo("ads123", (short) 1999, "azul", "Mazda");

        Multa multa1 = new Multa(12345, agente1, persona1, vehiculo1, fecha);

        MotivoMulta Mm = new MotivoMulta((short) 1234567, "", 120000);

        Licencia licencia1 = new Licencia(persona1, Categoria.C1, fecha);

        //System.out.println(licencia1.getPersona().getNombre());
        //System.out.println(licencia1.getPersona().getApellido());
        //System.out.println(licencia1.getPersona().getIdentificacion());
        Principal principal = new Principal(orgt);
        principal.setVisible(true);

        Agente[] agentes = {agente1, agente2};

        Vehiculo[] vehiculos = {vehiculo1};

        for (Agente agente : agentes) {
            orgt.agregarAgente(agente);

            //  .agregarProducto(producto);
        }

        for (Vehiculo vehiculo : vehiculos) {
            orgt.agregarVehiculo(vehiculo);

        }
        Licencia[] licencias = {licencia1};

        for (Licencia licencia : licencias) {
            orgt.agregarLicencia(licencia);

        }

        // TODO code application logic here
    }

}
